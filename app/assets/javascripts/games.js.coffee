# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


jQuery ->
  $('.home-team-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
	
  $('.away-team-select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'	