# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :game do
    home_team 1
    away_team 1
    created_at "2013-05-05 21:55:03"
    updated_at "2013-05-05 21:55:03"
    status 1
    game_date "MyString"
  end
end
